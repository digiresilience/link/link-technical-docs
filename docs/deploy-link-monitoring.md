# Deploy the Link monitoring stack

The Link monitoring stack consists of:

* Debian GNU/Linux Stretch 9
* Docker
* Wireguard
* Grafana
* Prometheus
* Matrix

## Introduction and Setup

Before continuing, please make sure you have completed the steps in the [Link deployment guide](deploy-link.md).

Begin by cloning the [reference monitoring deployment repository](https://gitlab.com/digiresilience/link/link-monitoring-deploy).

New to Ansible? See the [devops guidelines](https://gitlab.com/guardianproject-ops/ops-guidelines/blob/master/README.md#ansible) provided by our technology partner The Guardian Project.
