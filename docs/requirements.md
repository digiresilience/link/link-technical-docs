### Knowledge

These are the things you should familiarize yourself with before deploying Link:

* How to provision Linux servers with the hosting provider of your choice
* How to run Ansible playbooks (writing Ansible code is optional)
* For troubleshooting, how to examine running Docker containers

### Hardware

#### For the Link stack
* A fresh Debian 64bit machine with a public ip address
    * System recommendations
        * 8GB+ RAM
* A domain name

#### For the Link monitoring stack
* A fresh Debian 64bit machine with a public ip address
    * System recommendations
        * 4GB+ RAM

### Software

* Python 3 on your local host
* Ansible >= 2.7 installed locally on the machine you will deploy from (aka, your dev machine) using python3
