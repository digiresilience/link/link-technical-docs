

## SSL issues
This application uses LetsEncrypt for SSL certificates. If the domain you are hosting the application is fronted by Cloudflare you will need to temporarily disable it during the provisioning process. Once you are able to connect to your instance it's fine/advisable to re-enable Cloudflare.

## Elastic Search Issues

### Symptoms

#### `SearchIndexJob` is failing

If in the system monitoring section of Zammad you see something like the following, then run through these diagnostic steps.

```
19 failing background jobs
Failed to run background job #1 'SearchIndexJob' 10 time(s) with 144 attempt(s).
```

1. Check that the elastic search container is running on the host

  ```
  sudo docker ps -a | grep elasticsearch
  ```

  If it is not running, then proceed to the section "Restarting ElasticSearch"

2. If it is already running, then check the scheduler logs

  ```
  sudo docker logs zammad_zammad-scheduler_1 --tail 100 -f
  ```

If you see something like the following, then proceed to the section "Reinstall ingest-attachment plugin"

```
Response:
#<UserAgent::Result:0x00007f6d99dd66f8 @success=false, @body="{\"error\":{\"root_cause\":[{\"type\":\"illegal_argument_exception\",\"reason\":\"pipeline with id [zammad250358057626] does not exist\"}],\"type\":\"illegal_argument_exception\",\"reason\":\"pipeline with id [zammad250358057626] does not exist\"},\"status\":400}", @data=nil, @code="400", @content_type=nil, @error="Client Error: #<Net::HTTPBadRequest 400 Bad Request readbody=true>!">
```


### Solutions

#### Restart ElasticSearch

```
sudo docker restart zammad_zammad-elasticsearch_1
sudo docker restart zammad_zammad-scheduler_1
```

Check the logs to make sure they start successfully.


#### Reinstall ingest-attachment plugin

From the host machine, exec a shell into the elasticsearch container:

```
sudo docker exec -it zammad_zammad-elasticsearch_1 bash
```

Then remove and re-install the ingest-attachment plugin:

```
./bin/elasticsearch-plugin remove ingest-attachment
./bin/elasticsearch-plugin install ingest-attachment
exit
```

Then restart the container

```
sudo docker restart zammad_zammad-elasticsearch_1
```

Then proceed to the solution "Rebuild the ElasticSearch index"

#### Rebuild the ElasticSearch index

From the host machine, exec a shell into the railsserver container as the zammad user:

```
sudo docker exec -it zammad_zammad-railsserver_1 bash -c 'su zammad -c bash'
```

Then rebuild the search index:

WARNING: this command can take a very long time to run. Perhaps start a tmux
session on the host before execing into the container.

```
bundle exec rake searchindex:rebuild
````
