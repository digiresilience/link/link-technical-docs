# Overview

**Link** from the Center for Digital Resilience is an ecosystem comprised of a number of open source tools that address challenges facing the civil society incident responder community.

This guide will walk you through deployment and setup of the Link stack. At the end of this process you will have a working, security-hardened Zammad help-desk application that incorporates custom Signal and WhatsApp channels for secure communication.

This document is intended for a technical audience that is familiar with Linux server administration and deployment tools (primarily Ansible). If you would like to run the Link stack but prefer to have someone else handle deployment, please contact us at [info@digiresilience.org](mailto:info@digiresilience.org?subject=Link%20hosting%20inquiry).
