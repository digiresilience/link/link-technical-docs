# How to deploy the Link stack

The Link stack consists of:

* Debian GNU/Linux Stretch 9
* Docker
* [zammad](https://github.com/zammad/zammad)
* [sigarillo](https://gitlab.com/digiresilience/link/sigarillo) for Signal integration
* [quepasa](https://gitlab.com/digiresilience/link/quepasa) for WhatsApp integration
* Automated LetsEncrypt certs

## Introduction and Setup

Begin by cloning the [reference deployment repository](https://gitlab.com/digiresilience/link/link-deploy).

New to Ansible? See the [devops guidelines](https://gitlab.com/guardianproject-ops/ops-guidelines/blob/master/README.md#ansible) provided by our technology partner The Guardian Project.

### Prerequisites

* A fresh Debian 64bit machine with a public ip address
* A domain name
* Python 3 on your local host
* [Ansible](http://docs.ansible.com/ansible/latest/intro_installation.html#installation)
  >= 2.7 installed locally on the machine you will deploy from (aka, your dev
  machine) using python3

### Dependencies

This reference repository contains only the playbooks and sample vars files necessary for deploying Link.

The actual Ansible roles that contain all the logic exist in their own Git repositories. You can find all of them under the [guardianproject-ops](https://gitlab.com/guardianproject-ops/) organizations on Gitlab.

The dependencies are defined in [requirements.yml](https://gitlab.com/digiresilience/link/link-deploy/master/blob/requirements.yml).

Before proceeding you will need to install the roles that we depend on locally with the ansible-galaxy command:

```
ansible-galaxy install -r requirements.yml -f
```

**Note:** For security reasons we choose to host all of our Ansible roles ourselves instead of using Ansible's Galaxy hosted service.

## Operational Information

### Settings

Before deploying you will need to fill in appropriate settings in two files
* [inventory/host_vars/ssh.yml](inventory/host_vars/ssh.yml) which contains information that will allow you to connect to your instance
* [inventory/host_vars/zammad.yml](inventory/host_vars/zammad.yml) which contains information necessary for the Zammad installation

### Secrets

The reference repository contains a sample, unencrypted secrets file ([vars/secrets_sample.yml](vars/secrets_sample.yml)). The secrets file includes things like database passwords, private keys, etc. We store our actual secrets.yml in Git in an encrypted form using [Ansible Vault](https://docs.ansible.com/ansible/2.4/vault.html#use-encrypt-string-to-create-encrypted-variables-to-embed-in-yaml) and we recommend you do the same. The password that protects these secrets should be shared securely (in person or via secure messaging) amongst your operations team.

**Read the secrets**:

In order for Ansible to read the encrypted values, you must place the securely shared password in a file.

```
vim .vault        # in the same 'ansible' directory this readme is in
                  # paste the password without a newline, save, and close
                  # beware that other text editors may add a newline automatically
chmod 600 .vault
```

Then, whenever you want to work with ansible, export a variable:

```
export ANSIBLE_VAULT_PASSWORD_FILE=.vault
```

and Ansible will be able to read the secrets.


**Edit the secrets file**:

```bash
ansible-vault edit vars/secrets.yml
```

### Hosts

We have these hosts

1. [`production`](inventory/production) - the production instance

Check out the hosts file for each of those (linked above) to see the hostnames
themselves.

### Playbooks

Everything you need to deploy Link exists in an Ansible playbook to ensure everything we do to a server is documented and reproducible.

Each playbook takes a `-i` param that indicates which hosts to execute against. In our reference repository we have only have one item in our server 'inventory' but you may find it useful to add others (a development server, for instance).

To limit a playbook to a specific hostname (often a good idea) you can add the `--limit foobar.example.com` flag.

The playbooks are idempotent (with the exception of some documented exceptions), which means running them on an already provisioned server will not change or break anything.

#### Playbook: deploy a new server

The following steps will allow you to deploy the Link stack to a fresh server. You can also run these commands against an existing server to ensure it is fully up to date.

1. Change the ansible_user variable in `./inventory/host_vars/your-zammad.domain.org.yml` to `root`.

2. Run the system baseline playbook:
```bash
ansible-playbook ./playbooks/system-baseline.yml  -i inventory/production
```

3. Change the ansible_user variable back to `admin`.

4. Run the Zammad playbook:
```bash
ansible-playbook ./playbooks/zammad.yml  -i inventory/production
```

If you see warnings while running the monitoring or Zammad playbooks about the 'ufw' package not being installed, it's OK to proceed.

#### Playbook: Update the Zammad configuration

If you have made some changes to your Zammad configuration:

```bash
ansible-playbook ./playbooks/zammad.yml  -i inventory/production --tags zammad
```

#### Playbook: Apply system updates

This will run apt update/upgrade and reboot the host if necessary.

```bash
ansible-playbook ./playbooks/system-updates.yml  -i inventory/production
```

### SSH quick access

Username: `admin`
Port: `22`
Allowed ssh keys: see [vars/ssh.yml](vars/ssh.yml)
